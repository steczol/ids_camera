#include "IDSVideoCapture/IDSVideoCapture.h"
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>


#include <string>
#include <vector>
#include <iostream>
#include <memory>

int processIDSVideoCapture()
{

    int numCamera = 0;
    std::unique_ptr<IDSVideoCapture> videoCapture = std::make_unique<IDSVideoCapture>(numCamera);

    // videoCapture->open(numCamera);
    if(videoCapture->isOpened())
    {
        std::cout << "Camera is opened" << std::endl;

        cv::namedWindow("test");
        cv::Mat img;
        bool running = true;
        while(running)
        {
            videoCapture->grab();
            videoCapture->retrieve(img);
            cv::imshow("test", img);
            int key = cv::waitKey(30);
            if(key == 'q')
                running = false;
        }



    } else {
        std::cout << "Camera connot be opened" << std::endl;
    }






    return 0;
}

int
main(int argc, char** argv)
{
    return processIDSVideoCapture();
}