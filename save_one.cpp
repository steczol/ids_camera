#include <iostream>
#include <ueye.h>

using namespace std;

HIDS hCam = 0;      // 0 for the next available camera. 1-254 to access by ID
SENSORINFO sInfo;
HWND hWndDisplay;

char* pcImageMemory;
int DisplayWidth, DisplayHeight;

void printSensorInfo(SENSORINFO& sInfo)
{
    std::cout << "--- Sensor info ---" << std::endl;
    std::cout << "SensorID: " << sInfo.SensorID << std::endl;
    std::cout << "SensorName: " << sInfo.strSensorName << std::endl;
    std::cout << "ColorMode: " << static_cast<int>(sInfo.nColorMode) << std::endl;
    std::cout << "MaxWidth: " << sInfo.nMaxWidth << std::endl;
    std::cout << "MaxHeight: " << sInfo.nMaxHeight << std::endl;
    std::cout << "MasterGain: " << sInfo.bMasterGain << std::endl;
    std::cout << "RGain: " << sInfo.bRGain << std::endl;
    std::cout << "GGain: " << sInfo.bGGain << std::endl;
    std::cout << "BGain: " << sInfo.bBGain << std::endl;
    std::cout << "GlobShutter: " << sInfo.bGlobShutter << std::endl;
    std::cout << "PixelSize: " << sInfo.wPixelSize << " x 0.01 um" << std::endl;
    std::cout << "UpperLeftBayerPixel: " << static_cast<int>(sInfo.nUpperLeftBayerPixel) << std::endl;
    std::cout << "Reserved: " << sInfo.Reserved << std::endl;

}

int main()
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Starts the driver and establishes the connection to the camera
    int err = is_InitCamera(&hCam, hWndDisplay);
    std::cout << "Camera inited: " << err << std::endl;
    // You can query information about the sensor type used in the camera
    is_GetSensorInfo(hCam, &sInfo);
    std::cout << "Got sensor info: " << err << std::endl;
    printSensorInfo(sInfo);

    // Saving the information about the max. image proportions in variables
    DisplayWidth = sInfo.nMaxWidth;
    DisplayHeight = sInfo.nMaxHeight;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Need to find out the memory size of the pixel and the colour mode
    int nColorMode;
    int nBitsPerPixel = 24;

    if (sInfo.nColorMode == IS_COLORMODE_BAYER)
    {
        std::cout << "ColorMode: Bayer" << std::endl;
        // For color camera models use RGB24 mode
        nColorMode = IS_CM_BGR8_PACKED;
        nBitsPerPixel = 24;
    }
    else if (sInfo.nColorMode == IS_COLORMODE_CBYCRY)
    {
        std::cout << "ColorMode: CBYCRY" << std::endl;
        // For CBYCRY camera models use RGB32 mode
        nColorMode = IS_CM_BGRA8_PACKED;
        nBitsPerPixel = 32;
    }
    else
    {
        std::cout << "ColorMode: MONO8" << std::endl;
        // For monochrome camera models use Y8 mode
        nColorMode = IS_CM_MONO8;
        nBitsPerPixel = 24;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    int nMemoryId;

    // Assigns a memory for the image and sets it active
    is_AllocImageMem(hCam, DisplayWidth, DisplayHeight, nBitsPerPixel, &pcImageMemory, &nMemoryId);
    std::cout << "Image memory allocated.\tpcImageMemory: " << static_cast<int>(*pcImageMemory) << ", nMemoryId: " << nMemoryId << std::endl;
    is_SetImageMem(hCam, pcImageMemory, nMemoryId);
    std::cout << "Image memory set.\tpcImageMemory: " << static_cast<int>(*pcImageMemory) << ", nMemoryId: " << nMemoryId << std::endl;

    // Acquires a single image from the camera
    is_FreezeVideo(hCam, IS_WAIT);
    
    unsigned char* imgBuffer;
    UINT sizeOfData = 1280 * 1024 * 3;
    // Read memory buffer
    IS_MEMORY_ACCESS memAccess;
    memAccess.u32Description = 1;
    memAccess.u32Offset = 0;
    memAccess.pu8Data = imgBuffer;
    memAccess.u32SizeOfData = sizeOfData;

    err = is_Memory(hCam, IS_MEMORY_READ, (void*)&memAccess, sizeof(memAccess));

    std::cout << "memory read? " << err << std::endl;

    // Parameter definition for saving the image file
    IMAGE_FILE_PARAMS ImageFileParams;
    // ImageFileParams.pwchFileName = L"./TestImage.bmp";   /// <-- Insert name and location of the image
    ImageFileParams.pwchFileName = L"./TestImage.jpg";   /// <-- Insert name and location of the image
    ImageFileParams.pnImageID = NULL;
    ImageFileParams.ppcImageMem = NULL;
    ImageFileParams.nQuality = 0;
    // ImageFileParams.nFileType = IS_IMG_BMP;
    ImageFileParams.nFileType = IS_IMG_JPG;

    // Saves the image file
    if (is_ImageFile(hCam, IS_IMAGE_FILE_CMD_SAVE, (void*)&ImageFileParams, sizeof(ImageFileParams)) == IS_SUCCESS)
    {
        cout << "An Image was saved" << endl;
    }
    else
    {
        cout << "something went wrong" << endl;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Releases an image memory that was allocated
    is_FreeImageMem(hCam, pcImageMemory, nMemoryId);

    // Disables the hCam camera handle and releases the data structures and memory areas taken up by the uEye camera
    is_ExitCamera(hCam);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    return 0;
}
