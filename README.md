# README #

## Content

### save_one.cpp
Save one frame from the camera as .jpg (can be easily set to other formats) using just the ueye-library and header provided.

### show_frames.cpp
Using OpenCV show the frame stream coming from the camera.

IDSVideoCapture module taken from:
https://github.com/jmlowenthal/idslib



## Get the drivers
https://en.ids-imaging.com/ids-software-suite.html
I got the ueye_4.93.0.989_amd64.run, run it as sudo
It will, among others, install two daemons: /usr/local/share/ueye/ueyeusbd and /usr/local/share/ueye/ueyeethd a library at /usr/lib/libueye_api.so and a header file at /usr/include/ueye.h
Check if the daemons are running, the second one can be stopped and disabled (no ethernet camera).
The first one must be running in order to connect to the camera.

I have not found any documentation besides the content of ueye.h

## Compile
save_one.cpp:
g++ save_one.cpp -o save_one -lueye_api

show_frames.cpp: